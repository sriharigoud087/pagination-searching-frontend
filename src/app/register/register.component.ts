import { Component, OnInit } from '@angular/core';
import { from } from 'rxjs';
import { FormsModule, FormGroup, FormControl } from '@angular/forms';
import { ServerDataService } from '../server-data.service';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
})
export class RegisterComponent implements OnInit {
  constructor(private serverData: ServerDataService) {}
  registerForm = new FormGroup({
    userName: new FormControl(''),
    phoneNumber: new FormControl(''),
    email: new FormControl(''),
    address: new FormControl(''),
    city: new FormControl(''),
    state: new FormControl(''),
    postalCode: new FormControl(''),
  });

  onSubmit() {
    alert('submitted');
    //console.log(this.registerForm.value);
    this.serverData.onSubmitData(this.registerForm.value);
  }
  ngOnInit(): void {}
}
