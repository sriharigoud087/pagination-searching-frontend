import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { ServerDataService } from '../server-data.service';
import {
  Directive,
  EventEmitter,
  Input,
  Output,
  QueryList,
  ViewChildren,
} from '@angular/core';
import { NgbPaginationModule } from '@ng-bootstrap/ng-bootstrap';
import { NgbPaginationConfig } from '@ng-bootstrap/ng-bootstrap';
import { MatTableDataSource } from '@angular/material/table';
@Component({
  selector: 'app-display',
  templateUrl: './display.component.html',
  styleUrls: ['./display.component.css'],
})
export class DisplayComponent implements OnInit {
  constructor(private serverDataService: ServerDataService) {}
  responseData;

  page = 1;
  pageSize = 4;
  collectionSize = 50;

  ResponseDataArray = [];

  ngOnInit() {
    this.refreshUsers();
  }
  responseusersDataArray = [];
  refreshUsers() {
    this.responseusersDataArray = [];
    this.ResponseDataArray = [];
    this.serverDataService.showPagination(this.page, this.pageSize).subscribe(
      (response) => {
        this.responseData = JSON.stringify(response);
        var ResponseData = JSON.parse(this.responseData);
        console.log(ResponseData[0].userName);
        ResponseData.forEach((element) => {
          this.ResponseDataArray.push(element);
        });
        this.responseusersDataArray = this.ResponseDataArray;
      },
      (error) => {
        alert(error);
      }
    );
  }

  FilteredPlayers;
  applyFilter(event: Event) {
    this.responseusersDataArray = [];
    this.ResponseDataArray = [];
    const filterValue = (event.target as HTMLInputElement).value;
    let filteredValue = filterValue.trim().toLowerCase();
    if (filteredValue.length == 0) this.refreshUsers();
    else {
      this.serverDataService.searching(filteredValue).subscribe(
        (response) => {
          this.responseData = JSON.stringify(response);
          var ResponseData = JSON.parse(this.responseData);
          ResponseData.response.forEach((element) => {
            this.ResponseDataArray.push(element);
          });
          this.responseusersDataArray = this.ResponseDataArray;
        },
        (error) => {
          alert(error);
        }
      );
    }
  }
}
