import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
@Injectable({
  providedIn: 'root',
})
export class ServerDataService {
  constructor(public httpClient: HttpClient) {}
  onSubmitData(formData) {
    this.httpClient
      .post(
        'http://localhost:3000/api/register/store',
        JSON.stringify(formData)
      )
      .subscribe(
        (response) => {
          console.log(response);
        },
        (error) => {
          alert(error);
        }
      );
  }
  showAll() {
    return this.httpClient.get('http://localhost:3000/api/register/index');
  }

  showPagination(pageno, pagesize) {
    //alert('pagination');
    let params = new HttpParams();
    params = params.append('pageNo', pageno);
    params = params.append('size', pagesize);

    return this.httpClient.get(
      'http://localhost:3000/api/register/getPagination?',
      {
        params: params,
      }
    );
  }

  searching(searchText) {
    let params = new HttpParams();
    params = params.append('Text', searchText);
    return this.httpClient.get('http://localhost:3000/api/register/Search?', {
      params: params,
    });
  }
}
